package com.canos.example.example2;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
  
@RestController
@RequestMapping("/converters")
final class DateTimeController {
  
	// http://localhost:8080/converters/date?date=2016-12-12
    @RequestMapping(value = "/date", method = RequestMethod.GET)
    public String processDate(@RequestParam("date") LocalDate date) {
        return date.toString();
    }
  
    // http://localhost:8080/converters/datetime?datetime=2007-12-03T10:15:30.222
    @RequestMapping(value = "/datetime", method = RequestMethod.GET)
    public String processDateTime(@RequestParam("datetime") LocalDateTime dateAndTime) {
        return dateAndTime.toString();
    }
}