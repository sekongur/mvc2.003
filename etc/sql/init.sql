CREATE DATABASE `spring-advanced` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE `spring-advanced`.`user` (
  `iduser` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`iduser`));
  
  GRANT ALL PRIVILEGES on `spring-advanced`.* to 'spring-course'@'localhost' identified by 'spring-course';